const axios = require('axios');

//alterar o link para o numero do boneco que desejar, mas não há motivos: Chansey é OP.
axios.get('https://pokeapi.co/api/v2/pokemon/113')
    .then(response => {
        var { name, height, weight, types } = response.data

        var type = [];
        types.forEach(element => {
            type.push(element.type.name);
        })

        console.log(name + " (" + type + "), " + weight + "kg, " + height + "cm.")
    })
    .catch(error => {
        console.log(error);
    })
