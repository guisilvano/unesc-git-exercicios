const express = require('express')
const app = express()
const fs = require('fs')

app.use(express.json())

var arquivo = fs.readFileSync('./gerador_jogador.json', 'utf-8')
var lista = JSON.parse(arquivo)

app.get("/", (rec, res) => {
    //deu branco em como contar as linhas do arquivo lol
    var rngNome = Math.floor(Math.random() * 18)
    var rngSobrenome = Math.floor(Math.random() * 28)
    var rngIdade = Math.floor(Math.random() * 23) + 17
    var rngPosicao = Math.floor(Math.random() * 8)
    var rngClube = Math.floor(Math.random() * 20)

    res.json(lista.nome[rngNome] + " " + lista.sobrenome[rngSobrenome] + " é um futebolista brasileiro de " + rngIdade + " anos que atua como " + lista.posicao[rngPosicao]
        + ". Atualmente defende o " + lista.clube[rngClube])
})

app.get("/jogador", (rec, res) => {
    var rngNome = Math.floor(Math.random() * 18)
    var rngIdade = Math.floor(Math.random() * 23) + 17

    if (rngIdade <= 22) {
        res.json(lista.nome[rngNome] + " é um jogador novato de " + rngIdade + " anos.")
    } else if (rngIdade <= 28) {
        res.json(lista.nome[rngNome] + " é um jogador profissional de " + rngIdade + " anos.")
    } else if (rngIdade <= 34) {
        res.json(lista.nome[rngNome] + " é um jogador veterano de " + rngIdade + " anos.")
    } else if (rngIdade <= 40) {
        res.json(lista.nome[rngNome] + " é um jogador master de " + rngIdade + " anos.")
    }
})

app.listen(8080, () => {
    console.log("Listening on port 8080")
})