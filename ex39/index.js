const express = require('express')
const app = express()
const oficina = require("./routers/oficina")

app.use(express.json())

app.use("/oficina", oficina)

app.listen (8080, () => {
    console.log ("Listening on port 8080")
})