const express = require('express')
const app = express()
const fs = require('fs')

app.get('/', function(req, res){
    //const file = './floresta.jpg'
    //res.download(file)

    res.sendfile("index.html")
});

app.get('/imagens/floresta.jpg', function(req, res){
    const file = './imagens/floresta.jpg'
    res.download(file)
});


app.listen (8080, () => {
    console.log ("Listening on port 8080")
})