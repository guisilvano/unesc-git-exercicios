const express = require('express')
const path = require('path')
const fs = require('fs')

const app = express()

app.use(express.urlencoded())

app.use("/", function(req, res, next){
    console.log("Acesso em: " + Date.now())
    res.sendFile(__dirname+'/index.html')
    next()
})

app.get("/", (req, res) => {
    console.log("teste")
})

app.post("/sent", (req, res) => {
    console.log("post")
    console.log(req.body.produto)
    
    file_path = `${__dirname}/produtos/data.txt`

    var fp = fs.readFileSync(file_path, "utf8")
    fp = fs.readFileSync(file_path, "utf8")
    fp = fs.writeFileSync(file_path, req.body.produto+"\n", {encoding: "utf8", flag: "a"})
})

app.use(express.static("produtos"))

app.listen(3000, function () {
    console.log('Escutando na porta 3000!')
});