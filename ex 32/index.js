const express = require('express')
const app = express()

app.use(express.urlencoded())
var bodyParser = require('body-parser')
var jsonParser = bodyParser.json()
var urlencodedParser = bodyParser.urlencoded({ extended: false })



app.use("/", function(req, res,next) {
    console.log("Acesso realizado " + Date.now())
    next()
})

app.get ("/", (rec,res) => {
    res.sendfile("login.html")
})

app.post("/sucesso", (req, res) => {
    res.send (res.send("Login efetuado!"))
})

app.post("/usuario", urlencodedParser, function (req, res) {
    console.log(req.body)

    if ((req.body.nome === "root") && (req.body.senha === "unesc2019"))
    {
        console.log ("login efetuado!")
        res.sendFile("sucesso.html", { root : __dirname})
 
    } else {
        console.log ("falha ao logar!")
        res.sendFile("404.html", { root : __dirname})
    }
})

app.listen (8080, () => {
    console.log ("Listening on port 8080")
})