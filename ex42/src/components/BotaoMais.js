import React from 'react'

export default class BotaoMais extends React.Component{
    
    handleClick = () => {
        this.props.aumenta()
    }

    render (){
        return(
            <button onClick={this.handleClick}>+</button>
        );
    }
}