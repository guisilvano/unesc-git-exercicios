import React from 'react'

export default class BotaoMenos extends React.Component{
    
    handleClick = () => {
        this.props.diminui()
    }

    render (){
        return(
            <button onClick={this.handleClick}>-</button>
        );
    }
}