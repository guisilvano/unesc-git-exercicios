import React from 'react'

export default class BotaoModo extends React.Component{
    
    handleClick = () => {
        this.props.mudaModo()
    }

    render (){
        return(
            <button onClick={this.handleClick}>Modo</button>
        );
    }
}