import React from 'react'
import BotaoMais from './BotaoMais'
import BotaoMenos from './BotaoMenos'

export default class Temperatura extends React.Component{

    constructor(props){
        super(props)

        this.state = {
            temperatura: this.props.temperatura
        }
    }

    aumenta = () => {
        this.setState({
            temperatura: Number(this.state.temperatura) + 1
        })
    }

    diminui = () => {
        this.setState({
            temperatura: Number(this.state.temperatura) - 1
        })
    }

    render (){
        return(
            <div>
                <BotaoMenos diminui={this.diminui}></BotaoMenos><BotaoMais aumenta={this.aumenta}></BotaoMais> Temperatura: {this.state.temperatura}ºC 
            </div>
        );
    }
}