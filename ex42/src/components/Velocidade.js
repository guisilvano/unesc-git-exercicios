import React from 'react'
import BotaoMais from './BotaoMais'
import BotaoMenos from './BotaoMenos'

export default class Velocidade extends React.Component{

    constructor(props){
        super(props)

        this.state = {
            velocidade: this.props.velocidade
        }
    }

    aumenta = () => {

        if (Number(this.state.velocidade < 3)) {
            this.setState({
                velocidade: Number(this.state.velocidade) + 1
            })
        }
    }

    diminui = () => {
        if (Number(this.state.velocidade > 1)) {
            this.setState({
                velocidade: Number(this.state.velocidade) - 1
            })
        }
    }

    render (){
        return(
            <div>
                <BotaoMenos diminui={this.diminui}></BotaoMenos><BotaoMais aumenta={this.aumenta}></BotaoMais> Velocidade {this.state.velocidade} 
            </div>
        );
    }
}