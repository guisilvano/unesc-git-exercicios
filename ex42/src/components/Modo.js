import React from 'react'
import BotaoModo from './BotaoModo'

export default class Modo extends React.Component{

    constructor(props){
        super(props)

        this.state = {
            modoAtual: this.props.modoAtual,
            modos: ["Ventilar", "Refrigerar", "Aquecer"],
            index: 0
        }
    }

    mudaModo = () => {

        this.setState({
            index: Number(this.state.index),

            modoAtual: this.state.modos[this.state.index],
        })

        if (this.state.index === 2) {
            this.setState({
                index: 0
            })
        } else {
            this.setState({
                index: Number(this.state.index)+1
            })
        }

    }

    render (){
        return(
            <div>
               <BotaoModo mudaModo={this.mudaModo}></BotaoModo> {this.state.modoAtual} 
            </div>
        );
    }
}