import React from 'react';
import './App.css';
import Temperatura from './components/Temperatura'
import Modo from './components/Modo'
import Velocidade from './components/Velocidade'



function App() {
	return (
		<div>

			<Temperatura temperatura='20'></Temperatura>
			<Modo modoAtual='Aquecer'></Modo>
			<Velocidade velocidade='1'></Velocidade> 

		</div>
	);
}

export default App;
